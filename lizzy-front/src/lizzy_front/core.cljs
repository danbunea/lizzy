(ns ^:figwheel-hooks lizzy-front.core
  (:require
   [goog.dom :as gdom]
   [reagent.core :as reagent :refer [atom]]
   [lizzy-front.hierarchy-controller :as controller]
   [lizzy-front.hierarchy-view :refer [screen-component]]
   [lizzy-front.hierarchy-model :refer [model]]
   ))

(defonce init (controller/init!))

(defn get-app-element []
  (gdom/getElement "app"))

(defn app [] [screen-component @model] )

(defn mount [el]
  (reagent/render-component [app] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
