(ns lizzy-api.dao.lizzy-dao
  (:require
            [korma.core :as k :refer [defentity table pk transform prepare has-many belongs-to select select* fields raw]]
            [lizzy-api.database :refer [db] ]
            )
  )


(declare  user subject vote)

(defentity user
           (table :users)
           (pk :id)
           ;(transform remove-extra-columns)
           ;(prepare prepare-full-text)
           ;(has-many state {:fk :country_code})
           ;(has-many poi {:fk :country_code})
           )

(defentity subject
           (table :subjects)
           (pk :id)
           )

(defentity vote
           (table :votes)
           (has-many subject {:fk :subject_id})
           (has-many user {:fk :user_id})
           )


(defn save
  ([table to-save]
   (save table to-save (select-keys to-save [:id] )))
  ([table to-save condition]
   (when (not (nil? to-save))
     (let [result
           (if (empty? condition)
             0
             (k/update table (k/set-fields to-save) (k/where condition))
             )]
       ;(println result condition)
       (if (zero? result)
         (let
           [id (:generated_key (k/insert table (k/values to-save)))]
           (if id
             (assoc to-save :id id)
             to-save
             )
           )
         to-save)
       ))))


(comment
  (save subject {:title "Cool Acceptance Test "})

  (select-keys {:title "Cool Acceptance Test Changed"} [:id] )

  (k/insert subject
            (k/values {:title "Cool Acceptance Test"})
            )
  (k/insert user
            (k/values {:username "Dan"})
            )
  (k/insert vote
            (k/values {:user_id 1 :subject_id 1})
            )

  )

