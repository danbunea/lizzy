(ns lizzy-api.tests-macros
  (:require
    [korma.db :refer [transaction rollback]]))


(defmacro rollback-test
  ([body]
   `(transaction
      (try ~body
           (finally (rollback))))))


