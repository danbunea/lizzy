(ns lizzy-api.features.hierarchy-feature
  (:require
    [clojure.test :refer :all]
    [lizzy-api.tests-macros :refer [rollback-test]]
    [lizzy-api.dao.lizzy-dao :refer [save user vote subject]]
    [lizzy-api.features.hierarchy-feature-data :as data]
    [lizzy-api.handler :refer [app]]
    [ring.mock.request :as ring-mock]
    [clojure.data.json :as json]
    ))


(defn parse-body [body]
  (json/read-str (slurp body) :key-fn keyword))

(deftest acceptance-test
  (rollback-test
    (let
      ; given two subjects and a vote
      [ s1 (save subject data/subject-1)
       s2 (save subject data/subject-2)
       dan (save user data/user-dan)
       sabin (save user data/user-sabin)
       dan-vote-s1 {:user_id (:id dan) :subject_id (:id s1) }
       dan-vote-s2 {:user_id (:id dan) :subject_id (:id s2) }
       sabin-vote-s1 {:user_id (:id sabin) :subject_id (:id s1) } ]
      ; when
      (let [{:keys [status body]} (-> (ring-mock/request :post "/api/vote")
                                      (ring-mock/json-body dan-vote-s1)
                                      app )
            expected-body dan-vote-s1]
        (is (= status 200))
        (is (= (parse-body body) expected-body)))
      (let [{:keys [status body]} (-> (ring-mock/request :post "/api/vote")
                                      (ring-mock/json-body dan-vote-s2)
                                      app )
            expected-body dan-vote-s2]
        (is (= status 200))
        (is (= (parse-body body) expected-body)))
      (let [{:keys [status body]} (-> (ring-mock/request :post "/api/vote")
                                      (ring-mock/json-body sabin-vote-s1)
                                      app )
            expected-body sabin-vote-s1]
        (is (= status 200))
        (is (= (parse-body body) expected-body)))
      (let [{:keys [status body]} (-> (ring-mock/request :delete "/api/vote")
                                      (ring-mock/json-body dan-vote-s2)
                                      app )
            expected-body {:deleted 1}]
        (is (= status 200))
        (is (= (parse-body body) expected-body)))

      (let [{:keys [status body]} (-> (ring-mock/request :get "/api/hierarchy")
                                      app )
            expected-body [(assoc s1 :votes [(:id sabin) (:id dan)])
                           (assoc s2 :votes [])]]
        (is (= status 200))
        (is (= (parse-body body) expected-body)))
      )
    ))

(comment
  (save user data/user-dan)
  (save vote data/vote-dan-subject-1)
  )