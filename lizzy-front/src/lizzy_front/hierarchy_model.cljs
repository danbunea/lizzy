(ns lizzy-front.hierarchy-model
  (:require [reagent.core :as r]
            ))

(def initial-model {:name "sabin" :surname "gurung"})
(defonce model (r/atom initial-model))
(defn ^:export jsmodel []
  (clj->js @model))
