(ns lizzy-api.dao.vote-dao-IT
  (:require [clojure.test :refer :all]
            [lizzy-api.tests-macros :refer [rollback-test]]
            [lizzy-api.features.hierarchy-feature-data :as data]
            [korma.core :as k]
            [lizzy-api.dao.lizzy-dao :refer [vote save subject user]]
            [lizzy-api.dao.vote-dao :as vote-dao]
            ))

(deftest save-a-vote
  (rollback-test
    (let
      ;given
      [s1 (save subject data/subject-1)
       dan (save user data/user-dan)
       expected-vote {:user_id (:id dan) :subject_id (:id s1)}
       inserted-vote-id nil]
      ;when
      (vote-dao/insert-vote expected-vote)
      ;then
      (is (= expected-vote (first (k/select vote
                                            (k/where expected-vote))
                                  )))
      )
   )
  )

(deftest delete-a-vote
  (rollback-test
    (let
      ;given
      [s1 (save subject data/subject-1)
       dan (save user data/user-dan)
       vote-to-insert {:user_id (:id dan) :subject_id (:id s1)}
       _ (vote-dao/insert-vote vote-to-insert)
       expected-vote ()
       ]
      ;when
      (vote-dao/delete-vote {:user_id (:id dan) :subject_id (:id s1)})
      (is (= expected-vote (k/select vote))))
      )
      ;then
    )

(comment
  (def s1 (save subject data/subject-1))
  (def dan (save user data/user-dan))
  (def vote-to-insert {:user_id (:id dan) :subject_id (:id s1)})
  (vote-dao/insert-vote vote-to-insert)
  (vote-dao/delete-vote {:user_id (:id dan) :subject_id (:id s1)})
  )

