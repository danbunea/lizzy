(ns lizzy-front.hierarchy-controller-should
  (:require
    [cljs.test :refer-macros [deftest is testing use-fixtures async]]
    [lizzy-front.hierarchy-model :refer [model]]
    [lizzy-front.services.http :refer [<GET <POST <DELETE]]
    [lizzy-front.hierarchy-data :as data]
    [lizzy-front.hierarchy-controller :as controller :refer [commit!]]
    )
  (:require-macros [cljs.core.async.macros :refer [go]])
  )

(use-fixtures :each {:before (fn [] (reset! model {}))})



(deftest read-subjects-from-the-server-and-add-them-to-the-model
  (async done                                               ;for the test to know when we're done
    (go                                                     ;async
      (let [expected-subjects [data/tema-1]
            expected {:subjects expected-subjects}]
        ;the
        (with-redefs [<GET #(go expected-subjects)]
                     ;when/then
                     (is
                       (= expected
                          ;await
                          (<! (controller/init!))))
                     (done)                                 ;testing finished
                     )))))

(deftest vote-should-send-post-request-to-server
  (async done
    (go
      (let [expected {:user_id 6 :subject_id 11}]
        (with-redefs [<POST #(go expected)
                      <GET #(go nil)]
                     ;when/then
                     (is
                       (= expected
                          ;await
                          (:voted (<! (controller/vote! (:user_id expected) (:subject_id expected))))
                         ))
                     (done)
                     )))))

(deftest unvote-should-send-delete-request-to-server
  (async done
    (go
      (let [expected {:deleted 1}]
        (with-redefs [<DELETE #(go expected)
                      <GET #(go nil)]
                     ;when/then
                     (is
                       (= expected
                          ;await
                          (:deleted (<! (controller/unvote! nil nil)))
                          ))
                     (done)
                     )))))


(comment
  (go (<! (controller/init!)))

  )