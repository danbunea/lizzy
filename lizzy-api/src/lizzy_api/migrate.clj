(ns lizzy-api.migrate
  (:require [migratus.core :as migratus]
            [lizzy-api.database :refer [db-config]])
  (:gen-class))


(def config {:store                :database
             :migration-dir        "migrations/"
             ;:init-script          "schema_radisson.sql"
             ;defaults to true, some databases do not support
             ;schema initialization in a transaction
             ;:init-in-transaction? false
             :migration-table-name "_migrations"
             :db                   db-config
             })

(defn -main [& args]
  (println "migrating ...")
  (println "config: " config)
  (println "obtaining migrations... ")
  (println "migrations: " (migratus/pending-list config))
  (println "running migrations... ")
  (migratus/migrate config)
  (println "migrated all")
  )

(comment
  ;(migratus/pending-list config)

  ;(migratus/create config "create user, subjects and votes")
  ;(migratus/create config "save full_search_indexes to elastic search index" :edn)

  (migratus/up config 20191031162011)
  (migratus/down config 20191031162011)

  )
