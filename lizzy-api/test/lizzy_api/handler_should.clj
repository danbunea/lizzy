(ns lizzy-api.handler-should
  (:require [clojure.test :refer :all]
            [lizzy-api.features.hierarchy-feature :refer [parse-body]]
            [lizzy-api.handler :refer [app]]
            [ring.mock.request :as ring-mock]
            [clojure.data.json :as json]
            [mock-clj.core :refer [with-mock]]
            [lizzy-api.use-cases.vote :as vote-use-case]
            [lizzy-api.use-cases.hierarchy :as hierarchy-use-case]
            ))


(deftest user-votes-a-subject
  (testing "Test POST request to /api/vote and returns expected response"
    (let [expected-body { :user_id 1 :subject_id 1 }]
      (with-mock [vote-use-case/execute-vote expected-body]
         (let [
               {:keys [status body]} (->
                                       (ring-mock/request :post "/api/vote")
                                       (ring-mock/json-body { :user_id 1 :subject_id 1 })
                                       app
                                       )
               ]
           (is (= status 200))
           (is (= (parse-body body) expected-body)) )
         )
      )
    )
  )

(deftest get-list-of-votes-count
  (testing "Test GET request to /api/hierarchy and returns expected response"
    (let [expected-body [{:id 1 :title "title 1" :votes [1]}
                         {:id 1 :title "title 2" :votes []}]]
      (with-mock [hierarchy-use-case/execute expected-body]
                 (let [
                       {:keys [status body]} (->
                                               (ring-mock/request :get "/api/hierarchy")
                                               app
                                               )
                       ]
                   (is (= status 200))
                   (is (= (parse-body body) expected-body)) )
                 )
      )
    )
  )

(deftest user-unvotes-a-subject
  (testing "Test DELETE request to /api/vote and returns expected response"
    (let [
          request-body { :user_id 1 :subject_id 1 }
          expected-body {:deleted 1}
          ]
      (with-mock [vote-use-case/execute-unvote expected-body]
                 (let [
                       {:keys [status body]} (->
                                               (ring-mock/request :delete "/api/vote")
                                               (ring-mock/json-body { :user_id 1 :subject_id 1 })
                                               app
                                               )
                       ]
                   (is (= status 200))
                   (is (= expected-body (parse-body body))))
                 )
      )
    )
  )
