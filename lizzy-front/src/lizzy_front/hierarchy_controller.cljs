(ns lizzy-front.hierarchy-controller
  (:require [lizzy-front.hierarchy-model :refer [model]]
            [lizzy-front.services.http :refer [<GET <POST <DELETE]]
            [cljs.core.async :refer [<!]])
  (:require-macros [cljs.core.async.macros :refer [go]])
  )


(defn commit! [value atom-to-change]
  (reset! atom-to-change value))

(defn init! []
  (go
    (-> @model
        ;(assoc :user {:id 6})
        (assoc :subjects (<! (<GET "http://localhost:3000/api/hierarchy")))
        (commit! model))))

(defn vote! [user-id subject-id]
  (go
    ; send post request
    (-> @model
        (assoc :voted (<! (<POST "http://localhost:3000/api/vote" {:user_id user-id :subject_id subject-id})))
        (assoc :subjects (<! (<GET "http://localhost:3000/api/hierarchy")))
        (commit! model))))

(defn unvote! [user-id subject-id]
  (go
    ; send delete request
    (-> @model
        (assoc :deleted (<! (<DELETE "http://localhost:3000/api/vote" {:user_id user-id :subject_id subject-id})))
        (assoc :subjects (<! (<GET "http://localhost:3000/api/hierarchy")))
        (commit! model))))

(comment
  (go
    (print (<! (vote! 6 8)))
    (print (<! (unvote! 6 8)))
    ;(print (<! (<GET "http://localhost:3000/api/hierarchy")))
    ;(print (<! (<POST "http://localhost:3000/api/vote" {:user_id 6 :subject_id 8})))
    ;(print (<! (<DELETE "http://localhost:3000/api/vote" {:user_id 6 :subject_id 8})))
    )

  @model
  (commit! {:name "dan" :surname "bunea"} model)
  )
