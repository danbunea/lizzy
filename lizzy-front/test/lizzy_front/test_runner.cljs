;; This test runner is intended to be run from the command line
(ns lizzy-front.test-runner
  (:require
    ;; require all the namespaces that you want to test
    [lizzy-front.core-test]
    [lizzy-front.hierarchy-controller-should]
    [lizzy-front.hierarchy-views-should]
    [figwheel.main.testing :refer [run-tests-async]]))

(defn -main [& args]
  (run-tests-async 5000))
