(ns lizzy-front.hierarchy-data)

(def tema-1 {:id 1 :title "tema 1" :description "Here are some details of tema 1" :votes [1, 2, 3]})
(def tema-2 {:id 2 :title "tema 2" :description "Here are some details of tema 2" :votes [2, 3]})
(def voter-of-tema-1 {:id 1})
