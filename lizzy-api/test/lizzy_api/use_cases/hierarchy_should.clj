(ns lizzy-api.use-cases.hierarchy-should
  (:require [clojure.test :refer :all]
            [lizzy-api.dao.hierarchy-dao :as hierarchy-dao]
            [lizzy-api.use-cases.hierarchy :as hierarchy-use-case]
            [mock-clj.core :refer [with-mock calls call-count]]
            ))

(deftest hiearchy-use-case-should-call-hierarchy-get-subjects
  (testing "hiearchy use case should call hierarchy get subjects"
    (with-mock [hierarchy-dao/get-subjects-with-votes nil]
               (hierarchy-use-case/execute)
               (is (= (call-count hierarchy-dao/get-subjects-with-votes) 1))
               )
    )
  )

(deftest hiearchy-dao-transform-should-collect-voters-in-list
  (testing "hiearchy dao transform should collect voters in list"
    (let
      ;given
      [
       subjects '(
                  {:id 109, :title "Tema 1", :description "Description 1", :link nil, :subject_id 109, :user_id 77}
                  {:id 110, :title "Tema 2", :description "Description 2", :link nil, :subject_id 110, :user_id 77}
                  {:id 110, :title "Tema 2", :description "Description 2", :link nil, :subject_id 110, :user_id 12}
                  {:id 115, :title "Tema 2", :description "Description 2", :link nil, :subject_id nil, :user_id nil}
                  )
       expected '({:id 109, :title "Tema 1", :description "Description 1", :link nil, :votes (77)}
                  {:id 110, :title "Tema 2", :description "Description 2", :link nil, :votes (12 77)}
                  {:id 115, :title "Tema 2", :description "Description 2", :link nil, :votes ()})
       ]
      ;then
      (is (= expected (hierarchy-dao/transform-votes subjects)))
      )
    )
  )
