(ns lizzy-api.use-cases.vote-should
  (:require [clojure.test :refer :all]
            [lizzy-api.use-cases.vote :as vote-use-case]
            [lizzy-api.dao.vote-dao :as vote-dao]
            [mock-clj.core :refer [with-mock calls]]
            ))

(deftest votes-should-call-vote-dao-function
  (testing "votes should call vote dao function"
    (let [
          vote {:user_id 1 :subject_id 1}
          ]
      (with-mock [vote-dao/insert-vote nil]
                 (vote-use-case/execute-vote vote)
                 (is (= (calls vote-dao/insert-vote)  [[vote]]))
                 )
      )
    )
  )

(deftest unvote-should-call-dao-delete-vote-function
  (testing "when unvoting vote dao-delete-vote function"
    (let [
          unvote {:user_id 1 :subject_id 1}
          ]
      (with-mock [vote-dao/delete-vote nil]
                 (vote-use-case/execute-unvote unvote)
                 (is (= (calls vote-dao/delete-vote)  [[unvote]]))
                 )
      )
    )
  )

