(ns lizzy-api.dao.hierarchy-dao
  (:require [korma.core :as k]
            [lizzy-api.dao.lizzy-dao :refer [vote subject]]
            [schema.core :as s]))


(defn get-subjects-with-votes []
    (k/select subject
              (k/fields :id :title :description :link :votes.subject_id :votes.user_id)
              (k/join :left vote (= :votes.subject_id :subjects.id))
              )
  )

(defn transform-votes [subjects]
  (let
    [
     voters-reducer (fn [acc el]
                      (-> acc
                          (assoc-in [(:id el) :id] (:id el))
                          (assoc-in [(:id el) :title] (:title el))
                          (assoc-in [(:id el) :description] (:description el))
                          (assoc-in [(:id el) :link] (:link el))
                          (as-> ac (if (get-in ac [(:id el) :votes])
                                     ac
                                     (assoc-in ac [(:id el) :votes] '())))
                          (as-> ac (if (:user_id el)
                                     (update-in ac [(:id el) :votes] conj (:user_id el))
                                     ac))
                          )
                      )
     ]
    (->> subjects
         (reduce voters-reducer {})
         vals
         )
    )
  )



(comment
  ;(defn test-data []
  ;  '(
  ;    {:id 109, :title "Tema 1", :description "Description 1", :link nil, :subject_id 109, :user_id 77}
  ;    {:id 110, :title "Tema 2", :description "Description 2", :link nil, :subject_id 110, :user_id 7}
  ;    {:id 110, :title "Tema 2", :description "Description 2", :link nil, :subject_id 110, :user_id 12}
  ;    {:id 110, :title "Tema 2", :description "Description 2", :link nil, :subject_id 110, :user_id 77}
  ;    {:id 105, :title "Tema 2", :description "Description 2", :link nil, :subject_id 105, :user_id nil}
  ;    {:id 105, :title "Tema 2", :description "Description 2", :link nil, :subject_id 105, :user_id 88}
  ;    {:id 115, :title "Tema 2", :description "Description 2", :link nil, :subject_id nil, :user_id nil}
  ;    )
  ;  )
  (->>
    ;(test-data)
    (transform-votes)
    (sort #(compare (count (:votes %2)) (count (:votes %1))))
    )

  ;(save subject data/subject-1)
  ;(save subject data/subject-2)
  ;(save user data/user-dan)

  )
