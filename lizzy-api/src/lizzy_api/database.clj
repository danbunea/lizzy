(ns lizzy-api.database
  (:require [korma.db :as korma]
            [korma.core :as k])
  )

(def db-config
  {:classname "com.mysql.cj.jdbc.Driver"
   :subprotocol "mysql"
   :user "root"
   :password "qwerasdf"
   :subname "//localhost:3307/lizzy"}
  )

(def db-connection-info
  (korma/mysql db-config))

(korma/defdb db db-connection-info)

(comment
  ;(k/exec-raw "")
  (k/exec-raw ["SELECT 1" []] :results)
  )

