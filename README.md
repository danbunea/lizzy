# lizzy


Outside in TDD , front-end and backend exercise in ClojureScript/Clojure 

Generate front-end project:
```
lein new figwheel-main lizzy -- --reagent
```
Generate the backend project:
```
lein new compojure-api lizzy-api +clojure-test
```
# Exercise 1 - Setup database using migrations

## Problem

### Data
Mysql, compojure-api, migratus

### Unknown
SETUP Project and database

### Conditions
Through TDD and migrations

### Plan

#### Setup project
    [x] Create
    [+] Add libraries
        [+] Migratus
        [+] MySQL connector
        [+] sql korma
    [x] Setup the db
        [x] docker-compose
    [x] database.cljs
    [x] Migration 1 IT
        [x] migrate-up
            [x] has tables
        [x] migrate-down
            [x] empty db
            
## Check results
    [x] database checked

# Exercise 2 Feature - Subject Hierarchy
## Problem -  
### Data : We have users and subjects  
### Unknown : The list of subjects ordered by number of votes.  
### Conditions : User can vote for one or more subjects one time  

## Plan :
    [x] Acceptance Test - 
   
   **Given** a user dan and two subjects tema 1 and tema 2  
   And the user voted for tema 1  
   **When** reading the hierarchy  
   **Then**  
   tema1 should be first and have 1 vote  
   tema 2 last, 0 votes 
    
    [x] Handler Test
        [x] Post votes
        [x] Get hierarchy
    [x] use cases
        [x] vote should
        [x] hierarchy should
    [x] dao
        [x] vote dao IT 
            [x] save vote
        [x] hierarchy dao IT 
            [x] find subjects with number of votes
            
## Check results
    [x] endpoints checked


# Exercise 3 Feature - Vote/Unvote a subject
## Problem -  
### Data : We have users ,subjects and votes  
### Unknown : Vote/Unvote subject  
### Conditions : User can vote for a subject he hasn't voted or unvote something he has voted  

## Plan :
    [x] Acceptance Test - 
   
   **Given** a user dan and two subjects tema 1 and tema 2  
   And the user voted for tema 1  
   He can only vote for tema 2 and unvote for tema 1  
   **When** voting tema 2 and unvoting tema 1
   **Then** reading teh hierarchy 
   tema2  should be first and have 1 vote  
   tema 1 last, 0 votes
    
    [x] Handler Test
        [x] Delete vote
        [x] Get hierarchy - includes the votes not the number
    [x] use cases
        [x] vote should
            [x] unvote
        [x] hierarchy should
            [x] retrieve subjects and votes not number of votes
    [x] dao
        [x] vote dao IT 
            [x] delete vote
        [x] hierarchy dao IT 
            [x] find subjects with votes, not number of votes
            
## Check results
    [x] endpoints checked



