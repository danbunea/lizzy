(ns lizzy-api.dao.vote-dao
  (:require [korma.core :as k]
            [lizzy-api.dao.lizzy-dao :refer [vote save]]
    )
  )

(defn insert-vote [vote-body]
  (save vote vote-body)
  )

(defn delete-vote [vote-body]
    {:deleted (k/delete vote (k/where vote-body))}
  )
