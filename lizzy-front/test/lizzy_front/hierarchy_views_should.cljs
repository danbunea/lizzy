(ns lizzy-front.hierarchy-views-should
  (:require
    [cljs.test :refer-macros [deftest is testing use-fixtures]]
    [cljsjs.enzyme]
    [lizzy-front.hierarchy-view :as views]
    [lizzy-front.hierarchy-controller :as controller]
    [reagent.core :as r]
    [lizzy-front.hierarchy-data :as data]))


(deftest render-the-main-screen
  (testing "render with subjects"
    (let [component [views/screen-component {
                                             :user data/voter-of-tema-1
                                             :subjects [data/tema-1 data/tema-2]
                                             }]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]
      (is (= 1 (-> mounted (.find ".hierarchy") .-length)))

      (is (= 1 (-> mounted (.find "thead") .-length)))
      (is (= 1 (-> mounted (.find "tbody") .-length)))
      (is (= 2 (-> mounted (.find ".subject") .-length)))

      (is (= (:title data/tema-1) (-> mounted (.find ".subject") (.at 0)  (.find ".subject_title") .getDOMNode .-innerText)))
      (is (= (str (count (:votes data/tema-1)))
             (-> mounted (.find ".subject") (.at 0)  (.find ".subject_votes") .getDOMNode .-innerText)))

      ; unvote button exists if user is a voter
      (is (= 1 (-> mounted (.find ".subject") (.at 0) (.find ".subject_action") (.find ".btn-unvote") .-length)))
      (is (= 0 (-> mounted (.find ".subject") (.at 0) (.find ".subject_action") (.find ".btn-vote") .-length)))

      ; vote button exists if user is not a voter
      (is (= 0 (-> mounted (.find ".subject") (.at 1) (.find ".subject_action") (.find ".btn-unvote") .-length)))
      (is (= 1 (-> mounted (.find ".subject") (.at 1) (.find ".subject_action") (.find ".btn-vote") .-length)))
      )
    )
  )


(deftest clicking-vote-unvote-should-call-controller-functions
  (let [component [views/screen-component {
                                           :user data/voter-of-tema-1
                                           :subjects [data/tema-1 data/tema-2]
                                           }]
        mounted (->> (r/as-element component)
                     (.mount js/enzyme))
        invocations (atom [])]

    (with-redefs [controller/unvote! #(swap! invocations conj [%1 %2])]
                 (testing "clicking unvote should call controller/vote functions"
                   (reset! invocations [])
                   (-> mounted (.find ".subject") (.at 0) (.find ".subject_action") (.find ".btn-unvote") (.simulate "click"))
                   (is (= [[(:id data/voter-of-tema-1) (:id data/tema-1)]] @invocations))
                   )
                 )

    (with-redefs [controller/vote! #(swap! invocations conj [%1 %2])]
                 (testing "clicking vote should call controller/vote functions"
                   (reset! invocations [])
                   (-> mounted (.find ".subject") (.at 1) (.find ".subject_action") (.find ".btn-vote") (.simulate "click"))
                   (is (= [[(:id data/voter-of-tema-1) (:id data/tema-2)]] @invocations))
                   )
                 )
    )
  )
