 (defproject lizzy-api "0.1.0-SNAPSHOT"
   :description "FIXME: write description"
   :dependencies [
                  [org.clojure/clojure "1.10.0"]
                  [metosin/compojure-api "2.0.0-alpha30"]
                  [ring-cors "0.1.13"]
                  ;database
                  [korma "0.4.3"]
                  [mysql/mysql-connector-java "8.0.12"]
                  ; migrations
                  [migratus "1.2.2"]
                  [http-kit.fake "0.2.1"]
                  [ring/ring-mock "0.4.0"]
                  [org.clojure/data.json "0.2.6"]
                  [mock-clj "0.2.0"]
                  ]
    :ring {:handler lizzy-api.handler/app}
   :uberjar-name "server.jar"
   :profiles {:dev {:dependencies [[javax.servlet/javax.servlet-api "3.1.0"]
                                  [ring/ring-mock "0.3.2"]]
                   :plugins [[lein-ring "0.12.5"]]}})
