(ns lizzy-api.handler
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [compojure.route :as route]
            [lizzy-api.use-cases.vote :as vote-use-case]
            [lizzy-api.use-cases.hierarchy :as hierarchy-use-case]
            [schema.core :as s]
            [ring.middleware.cors :refer [wrap-cors]]))

(def app
  (-> (api
        {:swagger
         {:ui   "/"
          :spec "/swagger.json"
          :data {:info {:title       "Lizzy-api"
                        :description "Compojure Api example"}
                 :tags [{:name "api", :description "some apis"}]}}}

        (context "/api" []
          :tags ["api"]

          (POST "/vote" []
            :return vote-use-case/VoteResponse
            :body [vote vote-use-case/VoteBody]
            :summary "Create a vote"
            (ok (vote-use-case/execute-vote vote)))

          (DELETE "/vote" []
            :return vote-use-case/UnVoteResponse
            :body [vote vote-use-case/VoteBody]
            :summary "delete a vote"
            (ok (vote-use-case/execute-unvote vote)))

          (GET "/hierarchy" []
            :return hierarchy-use-case/Response
            :summary "Get list of all votes"
            (ok (hierarchy-use-case/execute)))

          (undocumented
            (route/not-found (ok {:not "found"})))
          ))
      (wrap-cors
        :access-control-allow-origin [#".*"]
        :access-control-allow-headers ["Origin" "X-Requested-With"
                                       "Content-Type" "Accept"]
        :access-control-allow-methods [:get :put :post :delete :options]
        )

      ))
