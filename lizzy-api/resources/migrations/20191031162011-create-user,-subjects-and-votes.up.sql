CREATE TABLE subjects (
  id int(11) NOT NULL AUTO_INCREMENT,
  title varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  description text COLLATE utf8mb4_unicode_ci,
  link varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  time_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
--;;
CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  passwd varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  time_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
--;;
CREATE TABLE votes (
  subject_id int(11) NOT NULL,
  user_id int(11) NOT NULL,
  PRIMARY KEY (subject_id,user_id),
  KEY user_id (user_id),
  CONSTRAINT votes_ibfk_1 FOREIGN KEY (subject_id) REFERENCES subjects (id),
  CONSTRAINT votes_ibfk_2 FOREIGN KEY (user_id) REFERENCES users (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
