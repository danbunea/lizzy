(ns lizzy-api.use-cases.hierarchy
  (:require [schema.core :as s]
            [lizzy-api.dao.hierarchy-dao :as hierarchy-dao]
            ))

(s/defschema Response [{
                        :id s/Num
                        :title s/Str
                        (s/optional-key :description) (s/maybe s/Str)
                        (s/optional-key :link) (s/maybe s/Str)
                        :votes [s/Num]
                        }])

(defn execute []
  (->>
    (hierarchy-dao/get-subjects-with-votes)
    (hierarchy-dao/transform-votes)
    (sort #(compare (count (:votes %2)) (count (:votes %1))))
    )
  )
