(ns lizzy-api.features.hierarchy-feature-data
  (:require [clojure.test :refer :all]))

(def subject-1 {:title "Tema 1" :description "Description 1" :link nil} )
(def subject-2 {:title "Tema 2" :description "Description 2" :link nil} )

(def user-dan {:username "Dan" } )
(def user-sabin {:username "Sabin" } )
