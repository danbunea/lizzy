(ns lizzy-front.hierarchy-view
  (:require [lizzy-front.hierarchy-controller :as controller])
  )

(defn screen-component [state]
  [:div.container
   [:div.jumbotron.mt-3
    [:h1 (str "Hierarchy")]
    [:p (str "Simple app to submit and vote for Community Of Practice (COP) subjects")]
    ]
   [:div.p-3.hierarchy
    [:table.table.table-dark
     [:thead
      [:tr [:th {:scope "col"} "ID"] [:th {:scope "col"} "Subject"] [:th {:scope "col"} "Vote"] [:th {:scope "col"} "Action"]]
      ]
     [:tbody
      (for [subj (:subjects state)]
        ^{:key (:id subj)}
        [:tr.subject
         [:th {:scope "row"} (:id subj)]
         [:td.subject_title (:title subj)]
         [:td.subject_votes (count (:votes subj))]
         [:td.subject_action
          (if (some #{(get-in state [:user :id])} (:votes subj))
            [:button.btn.btn-sm.btn-danger.btn-unvote {
                                                       :on-click #(controller/unvote! (get-in state [:user :id]) (:id subj))
                                                       } "Unvote" ]
            [:button.btn.btn-sm.btn-success.btn-vote {
                                                       :on-click #(controller/vote! (get-in state [:user :id]) (:id subj))
                                                       } "Vote" ]
            )
          ]
         ]
        )
      ]
     ]
    ]
   ])

