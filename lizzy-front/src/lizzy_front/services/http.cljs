(ns lizzy-front.services.http
  (:require
    [ajax.core :as aj :refer [GET POST ajax-request json-request-format json-response-format]]
    [cljs.core.async :refer [chan <! >! put! take!]]
    )
  (:require-macros [cljs.core.async.macros :refer [go]])
  )

(defn <GET [url]
  (let [ch (chan)]
    (aj/GET url {:handler (fn [response]
                            (put! ch response))})
    ch))

(defn <POST [url body]
  (let [ch (chan)]
    (ajax-request
      {:uri url
       :method :post
       :params body
       :handler (fn [response]
                  (put! ch response))
       :format (json-request-format )
       :response-format (json-response-format {:keywords? true})})
    ch))

(defn <DELETE [url body]
  (let [ch (chan)]
    (ajax-request
      {:uri url
       :method :delete
       :params body
       :handler (fn [response]
                  (put! ch response))
       :format (json-request-format )
       :response-format (json-response-format {:keywords? true})})
    ch))


