(ns lizzy-api.use-cases.vote
  (:require [schema.core :as s]
            [lizzy-api.dao.vote-dao :as vote-dao])
  )

(s/defschema VoteBody
  {
   :user_id s/Num
   :subject_id s/Num
   })

(s/defschema VoteResponse VoteBody)

(s/defschema UnVoteResponse { :deleted s/Num })


(defn execute-vote [body]
  (vote-dao/insert-vote body)
  )

(defn execute-unvote [body]
  (vote-dao/delete-vote body)
  )
