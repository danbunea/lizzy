(ns lizzy-api.dao.hierarchy-dao-IT
  (:require [clojure.test :refer :all]
            [lizzy-api.tests-macros :refer [rollback-test]]
            [lizzy-api.features.hierarchy-feature-data :as data]
            [korma.core :as k]
            [lizzy-api.dao.lizzy-dao :refer [vote save subject user]]
            [lizzy-api.dao.hierarchy-dao :as hierarchy-dao]
            [lizzy-api.dao.vote-dao :as vote-dao]
            ))

(deftest get-list-of-hotels-with-votes
  (rollback-test
    (let
      ;given
      [
       s1 (save subject data/subject-1)
       s2 (save subject data/subject-2)
       dan (save user data/user-dan)
       data-vote {:user_id (:id dan) :subject_id (:id s1)}
       _ (save vote data-vote)
       expected-hierarchy [
                           (-> s1
                               (assoc :subject_id (:id s1))
                               (assoc :user_id (:id dan))
                           )
                           (-> s2
                               (assoc :subject_id nil)
                               (assoc :user_id nil)
                               )
                           ]
       ]
      ;when
      ;then
      (is (= (hierarchy-dao/get-subjects-with-votes) expected-hierarchy))
      )
    )
  )
